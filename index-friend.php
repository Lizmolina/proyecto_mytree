<?php
  session_start();
  if ($_SESSION['user']){
    //user already logged in
    header('Location: /dashboard/Web1/proyecto/dashboard-friend.php');
  }

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'login':
        $message = 'User does not exists';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="css/estilo1.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <title>Friend Login</title>
</head>
<body background="img/bark-918457_1920.jpg">
<div class="container">
<div class="msg">
      <?php echo $message; ?>
    </div>
  <div class="d-flex justify-content-center">
    <div class="card   " >
      <div class="card-header">
        <h2>Friend Login</h2>
      </div>
      <div class="card-body">
      <form class="form" action="/dashboard/Web1/proyecto/login.php" method="POST" class="form-inline" role="form">
      <div class="form-group">
        <label class="sr-only" for="">Username</label>
        <input type="email" class="form-control" id="" name="username" placeholder="Your username">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Password</label>
        <input type="password" class="form-control" id="" name="password" placeholder="Your password">
      </div>

      <button  type="submit" class="btn btn-success btn-lg">Login</button>
         
       </form>
     
      </div>
       
     
      <div class="card-footer">
         <a class="label label-dark" href="/dashboard/Web1/proyecto/sing-in.php">Sing in</a>
      </div>
    </div>
  </div>
</div>

</body>
</html>

