<?php
  include('functions.php');
  
  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="assets/js/actions.js"></script>
  <title>Registro de Usuarios</title>
</head>
<body >
  <div class="jumbotron">
<div class="container">
<div class="card mb-3 text-center"  >
    <div class="msg" id="msg">
      <?php echo $message; ?>
    </div>
    <h1>Sing in Friend</h1>
    <form action="/dashboard/Web1/proyecto/createUser.php"  onsubmit="return validateUserForm();" method="POST" class="form" role="form">
    
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Name" require>
    </div>
    <div class="form-group col-md-6">
      <label for="lastname">Lastname</label>
      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname"  require>
    </div>
  </div>
  <div class="form-group col-md-6">
      <label for="phone_number">Phone Number</label>
      <input type="tel" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number" require>
    </div>
    <div class="form-group col-md-6">
      <label for="username">Email</label>
      <input type="email" class="form-control" id="username" name="username" placeholder="Email" require>
    </div>
  
  
  
  <input id="role" name="role" type="hidden" value="friend">
  
  <div class="form-group col-md-6">
      <label for="country">Country</label>
      <select id="country"  name="country" class="form-control">
        <option selected>Country</option>
        <option>Costa Rica</option>
        <option>Estado Unidos</option>
        <option>Salvador</option>
        <option>Honduras</option>
        <option>Nicaragura</option>
        <option>Guatemala</option>
        <option>Mexico</option>
        <option>Brazil</option>
        <option>Colombia</option>
        <option>Venezuela</option>
        <option>España</option>

      </select>
    </div>
    <div class="form-group col-md-6">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" require>
    </div>
    <div class="form-group col-md-12">
    <label for="address">Address</label>
    <input type="text" class="form-control" id="address" name="address" placeholder="Address" require>
  </div>
    
 
    
    <div class="form-group col-md-12">
    
      <button type="submit" class="btn btn-primary">Sign in</button>
    </div>
</form>
  
  </div>
  </div>
  </div>

</body>
</html>

