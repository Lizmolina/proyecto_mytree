<?php

include('tree.php');
  session_start();
  $user = $_SESSION['user'];
  if (!$user or $user['role'] == 'friend') {
    header('Location: /dashboard/Web1/proyecto/index.php');
  }
  
     
 
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
    
    <title>Friend List</title>
  </head>
  <body >
  <nav class="navbar nav-pills  navbar-expand-lg navbar-success bg-light ">
<a class="nav-link" href="/dashboard/Web1/proyecto/index.php">
    <img src="img/logo1.png" width="60" height="60" alt="">
  </a>
  
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/dashboard/Web1/proyecto/index.php"><h4>MyTree</h4>  
                <span class="sr-only">(current)</span>
              </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="/dashboard/Web1/proyecto/dashboard.php"><h4>Dashboard</h4> </a>
         
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="/dashboard/Web1/proyecto/listFriends.php"><h4>See Friends</h4></a>
         
        </li>
       
      </ul>
    
    </div>
  
</nav>

    
    <div class="container">
    <div class="d-flex justify-content-center">
    <table class="table  table-hover table-striped">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Phone</th>
      <th scope="col">Email</th>
      <th scope="col">Country</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php 
   $func= new Functions();
   $userFriends = $func->getUsersFriend();
    $usersHtml = "";
    foreach ($userFriends as $uf) {
        $usersHtml .= "<tr id='{$uf['id']}'><td>{$uf['name']} {$uf['lastname']}</td><td>{$uf['phone_number']}</td><td>{$uf['username']}</td><td>{$uf['country']}</td><td><a href='seeTree.php?id={$uf['id']}'>See Trees</a></td></tr> ";
    }
    echo $usersHtml;
    ?>

    
  </tbody>
</table>
    
    
                                  
    </div>
    </div>

  </body>
  </html>

 