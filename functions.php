<?php
include 'conexion.php';



class Functions{



/**
 * Inserts a new user  to the database
 *
 * @student An associative array with the student information
 */
function saveUser($user) {
  $conn= new Conexion();
  
  $sql = "INSERT INTO `users`( `name`, `lastname`, `phone_number`, `address`, `country`, `username`, `password`, `role`)
   VALUES ('{$user['name']}', '{$user['lastname']}', '{$user['phone_number']}' ,'{$user['address']}' ,'{$user['country']}' ,'{$user['username']}'
   ,'{$user['password']}', '{$user['role']}')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}


/**
 * Get all users from the database
 *
 */
function getUsers(){
  $conn = new Conexion();
  $sql = "SELECT * FROM users";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get one specific student from the database
 *
 * @id Id of the user
 */
function authenticate($username, $password){
  
  $conn = new Conexion();
  $sql = "SELECT * FROM users WHERE `username` = '$username' AND `password` = '$password'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
}

/**
 * Get all species from the database
 *
 */
function getSpecies(){
  $conn = new Conexion();
  $sql = "SELECT * FROM species";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get all trees from the database
 *
 */
function getTrees(){
  $conn = new Conexion();
  $sql = "SELECT t.id, t.id_species, s.name_species, t.height, t.phote FROM trees t INNER JOIN species s ON t.id_species = s.id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}
/**
 * Inserts a new user  to the database
 *
 * @student An associative array with the student information
 */
function saveUserTree($userTree) {
  $conn= new Conexion();
  
  $sql = "INSERT INTO `usertree`( `id_user`,  `id_tree`, `name`, `amount`)
   VALUES ('{$userTree['id_user']}', '{$userTree['id_tree']}', '{$userTree['name']}' ,'{$userTree['amount']}' )";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}
/**
 * Get all trees from the database
 *
 */
function getUserTree($id){
  $conn = new Conexion();
  $sql = "SELECT u.id, u.id_user, u.id_tree, u.name, u.amount, t.id, t.id_species, t.height, t.phote FROM usertree u INNER JOIN trees  t ON u.id_tree = t.id
  WHERE u.id_user = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get the friend type users from the database
 *
 */
function getUsersFriend(){
  $conn = new Conexion();
  $sql = "SELECT * FROM users WHERE role = 'friend' ";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get the Tree planted from the database
 *
 */
function getTreesPlanted(){
  $conn = new Conexion();
  $sql = "SELECT * FROM usertree";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}
/**
 * Get all trees from the database
 *
 */
function getTreesId($id){
  $conn = new Conexion();
  $sql = "SELECT t.id, t.id_species, s.name_species, t.height, t.phote FROM trees t INNER JOIN species s ON t.id_species = s.id WHERE t.id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Updates an existing userTree to the database
 *
 * @userTre An associative array with the userTree information
 */
function updateUserTree($userTree) {
  $conn = getConnection();
  $sql = "UPDATE usertree u INNER JOIN trees t
  ON u.id_tree = t.id
  set `t.id_species` = '{$userTree['id_species']}' , `height` = '{$userTree['height']}',
    `phote` = '{$userTree['phote']}' WHERE `id` = {$userTree['id']}";

  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Uploads an image to the server
 *
 * @inputName name of the input that holds the image in the request
 */
function uploadPicture($inputName){
  $fileObject = $_FILES[$inputName];

  $target_dir = "uploads/";
  $target_file = $target_dir . basename($fileObject["name"]);
  $uploadOk = 0;
  if (move_uploaded_file($fileObject["tmp_name"], $target_file)) {
    return $target_file;
  } else {
    return false;
  }
}
/**
 * Get all trees from the database
 *
 */
function getTreeUser($id){
  $conn = new Conexion();
  $sql = "SELECT u.id, u.id_user ,u.id_tree, t.id, t.id_species, t.height, s.id,  s.name_species, u.name, u.amount, t.phote FROM usertree u INNER JOIN trees t ON u.id_tree = t.id 
  Inner Join species s On s.id = t.id_species WHERE u.id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}



}






