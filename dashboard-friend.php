<?php
include('tree.php');

 session_start();
 
  $user = $_SESSION['user'];
 
  if (!$user or $user['role'] == 'admin') {
    header('Location: /dashboard/Web1/proyecto/index.php');
  }


  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'Tree buy was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the tree buy';
      break;
    }
  }
 
  ?>
  

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>

    <title>Dashboard Friend</title>
  </head>
  <body>
  <nav class="navbar fixed-top navbar-light bg-light">
  <a class="navbar-brand" href="#">Fixed top</a>
</nav>
<h1> Bienvenido <?php echo $user['name'] ?> </h1>
  <a href="/dashboard/Web1/proyecto/logout.php">Logout</a>

      <div class="container-fluid">
          <div class= "row">
              <div class="col-5">
              <div class="card border-success mb-3" style="width: 48rem;" >
                  <div class="card-header border-success mb-3">
                      <h3>Buy Tree</h3>
                  </div>
                  <div class="card-body">
                      <form action="/dashboard/Web1/proyecto/createUserTree.php"  method="POST" class="form" role="form">
                      <input id="id_user" name="id_user" type="hidden" value="<?php echo $user['id']; ?>"> 

                      <div class="form-group row">  
                          <label for="money" class="col-sm-3 col-form-label">Species:</label>
                          <div class="col-sm-7">
                              <select class="form-control" name="id_tree" id="id_tree" require>
                                  <option selected>Select to Species</option>
                                  <?php 
                                   $trees= new Tree();
                                   $trees::obtTrees();
                                   
                                   ?>
                                </select>
                            </div>
                            <a  data-toggle="modal" data-target="#exampleModalCenter "><img src="img/arbol.png" ></a>
                        </div>

                        <div class="form-group row ">
                            <label for="name" class="col-sm-3 col-form-label">Name:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="name"  name="name" placeholder="Name" require>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="money" class="col-sm-3 col-form-label">Donation:</label>
                            <div class="col-sm-7">
                                <input type="number" class="form-control" id="amount"  name="amount" placeholder="$$$$$" require>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="money" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-success btn-block pull-right">Buy</button>
                            </div>
                        </div>
                       </form>
                   </div>
                </div>


              </div>
              <div class="col-6"><h4 class="display-4">My Trees</h4>
              <div class="table-responsive">
  

              <table class="table">
                    
                    <thead class="table-success">
                        <tr>
                            <th scope="col">Species</th>
                            <th scope="col">Name</th>
                            <th scope="col">Amount Nonated</th>
                            <th scope="col">Phote</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                         $prue= new Functions();
                         $userTree = $prue->getUserTree($user['id']);
                         $treesHtml = "";
                        foreach ($userTree as $ut) {
                            $treesHtml .= "<tr><td>{$ut['id']}</td><td>{$ut['id_user']}</td><td>{$ut['id_tree']}</td></tr>";
                        }
                         echo $treesHtml;
                    ?>
                    </tbody>
                </table>
                </div>

            </div>
        </div>

                                   <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalCenterTitle">Tree Details</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table class="table table-light">
                                    <tbody>
                                        <tr>
                                            <td>Species</td>
                                            <td>Height</td>
                                            <td>Photes</td>
                                        </tr>
                                        <?php 
                                        $prueba= new Functions();
                                        $modal = $prueba::getTreesId($trees['id']);
                                        $modalHtml = "";
                                        foreach ($modal as $mod) {
                                            $modalHtml .= "<tr><td>{$mod['name_species']}</td><td>{$mod['height']}</td><td><img src='./dashboard/Web1/proyecto/img/" . $mod['phote'] . "'></td></tr>";
                                        }
                                        echo $modalHtml;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       
      
</body>
</html